﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ZahlensystemeUmwandeln
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // Durch Doppelklick auf die Textbox wird der Button 
        //überflüssig.

        private void tbxDezimal_TextChanged(object sender, EventArgs e)
       {
            //Es wird eine Methode aufgerufen.

            DezimalNachHexadezimal();
            DezimalNachDual();
        }

        private void tbxDualZahl_TextChanged(object sender, EventArgs e)
        {

        }

        private void DezimalNachDual()
        {
            int dezimalzahl = Convert.ToInt32(tbxDezimal.Text);

            string dualZahl = "";



            while (dezimalzahl >= 1)
            {



                int restDezimal = dezimalzahl - (dezimalzahl / 2) * 2;




                dezimalzahl = dezimalzahl / 2;



                dualZahl = restDezimal + dualZahl;




            }





            tbxDualZahl.Text = dualZahl;
        }

        private void DezimalNachHexadezimal()
        {
            // Der Wert der Textbox tbxDezimal wird nach Int32
            // konvertiert und zugewiesen an die lokale Variable
            // namens dezimalzahl vom Typ int
            int dezimalzahl = Convert.ToInt32(tbxDezimal.Text);

            // Ein leerer String wird zugewiesen an die lokale 
            // Variable namens hexadezimalzahl vom Typ string.
            // Der Typ muss string sein, da auch Buchstaben zum 
            // Wertebereich von Hexadezimalzahlen gehören.
            string hexadezimalZahl = "";

            // Die Buchstaben des Hexadezimalsystems werden
            // definiert. 
            string hexZiffern = "ABCDEF";

            // Solange der Wert von dezimalzahl > oder = 1 
            // ist, wird die Schleife ausgeführt.            
            while (dezimalzahl >= 1)
            {

                // Die Dezimalzahl wird geteilt durch die Basis  
                // (16) und anschließend multipliziert mit 16.
                // Weil es sich um eine Ganzzahldivision handelt, 
                // ist 24 / 16 nicht 1,5, sondern 1.
                // Es wird also abgerundet auf die Ganzzahl.
                int z = dezimalzahl / 16;

                //Es wird der Rest ermittelt.

                int restDezimal = dezimalzahl - z * 16;




                dezimalzahl = dezimalzahl / 16;


                // Die Restdezimalzahl wird Resthexadezimalzahl 
                // zugewiesen und in einen string konvertiert.

                string restHexadezimal = restDezimal.ToString();

                // Wenn restDezimal > 9, dann muss ein Buchstabe 
                // statt der Ziffern erscheinen.

                if (restDezimal > 9)
                {



                    restHexadezimal = hexZiffern.Substring(restDezimal - 10, 1);




                }



                hexadezimalZahl = restHexadezimal + hexadezimalZahl;
            }

            tbxHexadezimal.Text = hexadezimalZahl;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
