﻿namespace ZahlensystemeUmwandeln
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxDezimal = new System.Windows.Forms.TextBox();
            this.tbxHexadezimal = new System.Windows.Forms.TextBox();
            this.lblDezimal = new System.Windows.Forms.Label();
            this.lblHexadezimalzahl = new System.Windows.Forms.Label();
            this.lblDualzahl = new System.Windows.Forms.Label();
            this.tbxDualZahl = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbxDezimal
            // 
            this.tbxDezimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxDezimal.Location = new System.Drawing.Point(401, 28);
            this.tbxDezimal.Name = "tbxDezimal";
            this.tbxDezimal.Size = new System.Drawing.Size(203, 53);
            this.tbxDezimal.TabIndex = 0;
            this.tbxDezimal.TextChanged += new System.EventHandler(this.tbxDezimal_TextChanged);
            // 
            // tbxHexadezimal
            // 
            this.tbxHexadezimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxHexadezimal.Location = new System.Drawing.Point(401, 103);
            this.tbxHexadezimal.Name = "tbxHexadezimal";
            this.tbxHexadezimal.Size = new System.Drawing.Size(203, 53);
            this.tbxHexadezimal.TabIndex = 1;
            // 
            // lblDezimal
            // 
            this.lblDezimal.AutoSize = true;
            this.lblDezimal.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDezimal.Location = new System.Drawing.Point(31, 35);
            this.lblDezimal.Name = "lblDezimal";
            this.lblDezimal.Size = new System.Drawing.Size(237, 46);
            this.lblDezimal.TabIndex = 2;
            this.lblDezimal.Text = "Dezimalzahl";
            // 
            // lblHexadezimalzahl
            // 
            this.lblHexadezimalzahl.AutoSize = true;
            this.lblHexadezimalzahl.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHexadezimalzahl.Location = new System.Drawing.Point(31, 106);
            this.lblHexadezimalzahl.Name = "lblHexadezimalzahl";
            this.lblHexadezimalzahl.Size = new System.Drawing.Size(324, 46);
            this.lblHexadezimalzahl.TabIndex = 3;
            this.lblHexadezimalzahl.Text = "Hexadezimalzahl";
            // 
            // lblDualzahl
            // 
            this.lblDualzahl.AutoSize = true;
            this.lblDualzahl.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDualzahl.Location = new System.Drawing.Point(31, 182);
            this.lblDualzahl.Name = "lblDualzahl";
            this.lblDualzahl.Size = new System.Drawing.Size(175, 46);
            this.lblDualzahl.TabIndex = 5;
            this.lblDualzahl.Text = "Dualzahl";
            // 
            // tbxDualZahl
            // 
            this.tbxDualZahl.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxDualZahl.Location = new System.Drawing.Point(401, 179);
            this.tbxDualZahl.Name = "tbxDualZahl";
            this.tbxDualZahl.Size = new System.Drawing.Size(203, 53);
            this.tbxDualZahl.TabIndex = 4;
            this.tbxDualZahl.TextChanged += new System.EventHandler(this.tbxDualZahl_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 377);
            this.Controls.Add(this.lblDualzahl);
            this.Controls.Add(this.tbxDualZahl);
            this.Controls.Add(this.lblHexadezimalzahl);
            this.Controls.Add(this.lblDezimal);
            this.Controls.Add(this.tbxHexadezimal);
            this.Controls.Add(this.tbxDezimal);
            this.Name = "Form1";
            this.Text = "Zahlensysteme umwandeln";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxDezimal;
        private System.Windows.Forms.TextBox tbxHexadezimal;
        private System.Windows.Forms.Label lblDezimal;
        private System.Windows.Forms.Label lblHexadezimalzahl;
        private System.Windows.Forms.Label lblDualzahl;
        private System.Windows.Forms.TextBox tbxDualZahl;
    }
}

